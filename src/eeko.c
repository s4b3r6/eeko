#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "linenoise.h"
#include "colors.h"

enum choices {
  ATTACK,
  DEFEND,
  RUN,
  QUIT,
};

void display_choices(void) {
  colors_256(34);
  printf("%s", "(A)ttack | ");
  colors_256(102);
  printf("%s", "(D)efend | ");
  colors_256(178);
  printf("%s", "(R)un | ");
  colors_256(196);
  printf("%s", "(Q)uit\n");
  colors_reset();
}

int get_choice(const char* prompt, int prompt_color) {
  // x will be the character we care about.
  char x = 0;
  // line is a dynamic array handled by linenoise.
  char* line;
  while(x != 'a' &&
        x != 'A' &&
        x != 'D' &&
        x != 'd' &&
        x != 'R' &&
        x != 'r' &&
        x != 'q' &&
        x != 'Q') {
    colors_256(prompt_color);
    fflush(stdout);
    line = linenoise(prompt);
    colors_reset();
    // We only care about the first character.
    x = line[0];
    // Always free our memory!
    // Could use libc free here if we use libc malloc...
    // But let's just be flexible.
    linenoiseFree(line);
  }
  // Turn our captured character into the enum value
  // we expect.
  switch(x) {
    case('A'):
    case('a'):
      return ATTACK;
    case('D'):
    case('d'):
      return DEFEND;
    case('R'):
    case('r'):
      return RUN;
    case('Q'):
    case('q'):
      return QUIT;
    default:
      // Should never reach here!
      // If it does... Then the while loop above has failed
      // somehow... So crash the program.
      assert(0 == 1);
      return -1;
  }
}

struct enemy {
  unsigned int attack;
  unsigned int defend;
  bool run;
};

struct player {
  int health;
  unsigned int attack;
  unsigned int defend;
  int gold;
};

struct player new_player(void) {
  struct player p;
  p.health = 100;
  p.attack = 1;
  p.defend = 1;
  p.gold = 0;

  return p;
}

void dump_player(struct player* p) {
  printf("%s%d\n%s%d\n%s%d\n%s%d\n",
    "Health: ", p->health,
    "Gold: ", p->gold,
    "Attack: ", p->attack,
    "Defend: ", p->defend);
}

int main(int argc, char* argv[]) {
  colors_clear();
  //Create a new player
  struct player p = new_player();

  //Display the player
  dump_player(&p);
  // Show our choices
  display_choices();

  //Run story

  //Run battle
  int choice = get_choice("> ", 74);
  printf("%d\n", choice);
}

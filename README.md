# eeko

A simple console-based game.

---

## Status

Nowhere. The bare structures for interacting with the CLI are still being built.

There is nothing to see here.

---

## License

As close to Public Domain as my nation allows, but there are some copyrights I can't surrender, so this license will give you an unlimited license.

See [LICENSE](LICENSE) for full legal code.

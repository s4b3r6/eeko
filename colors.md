# Colors API

Colors will be referred to by their ANSI names.

Unless specified, the functions do *not* flush stdout.

## Usage

A quick example:

```
colors_256(4);
printf("%s". "This is blue!");
colors_reset();
```

## Functions

```colors_clear(void)``` - Clears the console, this also flushes stdout.

```colors_256(unsigned int num)``` - This sets the text color on stdout to the given 256bit color. If given an invalid range, it *will* crash using ```assert```.

```fcolors_256(FILE* out, unsigned int num)``` - This sets the text color on the given file to the given 256bit color. If given an invalid range, it *will* crash using ```assert```.

```colors_reset(void)``` - This function removes any styling currently on stdout. This also flushes stdout.

```fcolors_reset(FILE* out)``` - This function removes any styling currently on the given file.

```colors_black(void)``` - This sets the text color on stdout to black.

```fcolors_black(FILE* out)``` - This sets the text color on the given file to black.

```colors_red(void)``` - This sets the text color on stdout to red.

```fcolors_red(FILE* out)``` - This sets the text color on the given file to red.

```colors_green(void)``` - This sets the text color on stdout to green.

```fcolors_green(FILE* out)``` - This sets the text color on the given file to green.

```colors_yellow(void)``` - This sets the text color on stdout to yellow.

```fcolors_yellow(FILE* out)``` - This sets the text color on the given file to yellow.

```colors_blue(void)``` - This sets the text color on stdout to blue.

```fcolors_blue(FILE* out)``` - This sets the text color on the given file to blue.

```colors_magenta(void)``` - This sets the text color on stdout to magenta.

```fcolors_magenta(FILE* out)``` - This sets the text color on the given file to magenta.

```colors_cyan(void)``` - This sets the text color on stdout to cyan.

```fcolors_cyan(FILE* out)``` - This sets the text color on the given file to cyan.

```colors_white(void)``` - This sets the text color on stdout to white.

```fcolors_white(FILE* out)``` - This sets the text color on the given file to white.

```colors_bright_black(void)``` - This sets the text color on stdout to bright black.

```fcolors_bright_black(FILE* out)``` - This sets the text color on the given file to bright black.

```colors_bright_red(void)``` - This sets the text color on stdout to bright red.

```fcolors_bright_red(FILE* out)``` - This sets the text color on the given file to bright red.

```colors_bright_green(void)``` - This sets the text color on stdout to bright green.

```fcolors_bright_green(FILE* out)``` - This sets the text color on the given file to bright green.

```colors_bright_yellow(void)``` - This sets the text color on stdout to bright yellow.

```fcolors_bright_yellow(FILE* out)``` - This sets the text color on the given file to bright yellow.

```colors_bright_blue(void)``` - This sets the text color on stdout to bright blue.

```fcolors_bright_blue(FILE* out)``` - This sets the text color on the given file to bright blue.

```colors_bright_magenta(void)``` - This sets the text color on stdout to bright magenta.

```fcolors_bright_magenta(FILE* out)``` - This sets the text color on the given file to bright magenta.

```colors_bright_cyan(void)``` - This sets the text color on stdout to bright cyan.

```fcolors_bright_cyan(FILE* out)``` - This sets the text color on the given file to bright cyan.

```colors_bright_white(void)``` - This sets the text color on stdout to bright white.

```fcolors_bright_white(FILE* out)``` - This sets the text color on the given file to bright white.

## Unstable

These functions can change at any time, and are ***not*** part of the API. However you might find them helpful to run or examine when constructing a program.


``` colors_unstable_show_256(void)``` - This function displays all 256 colors that can be used with ```colors_256``` and ```fcolors_256```.

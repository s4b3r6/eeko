#!/bin/sh -x

git submodule foreach git pull origin master
cp linenoise/linenoise.h src
cp linenoise/linenoise.c src
musl-gcc -static -Wall -std=c99 src/eeko.c src/linenoise.c -Isrc -o eeko
rm src/linenoise.c
rm src/linenoise.h
